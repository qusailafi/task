package com.movieapp.qusai.ui.star_cast;

import java.io.Serializable;
import java.util.ArrayList;

public class StarCaseResponse implements Serializable {

    int id;
    ArrayList<StarCastObject>cast;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<StarCastObject> getCast() {
        return cast;
    }

    public void setCast(ArrayList<StarCastObject> cast) {
        this.cast = cast;
    }
}
