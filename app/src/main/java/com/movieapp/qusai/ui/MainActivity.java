package com.movieapp.qusai.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.movieapp.qusai.R;
import com.movieapp.qusai.ui.base.BaseActivity;
import com.movieapp.qusai.ui.login.LoginActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        goToLoginPage();
    }

    private void goToLoginPage(){
    new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
    },2000);
    }
}