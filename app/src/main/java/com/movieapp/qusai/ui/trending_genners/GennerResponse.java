package com.movieapp.qusai.ui.trending_genners;

import java.io.Serializable;
import java.util.ArrayList;

public class GennerResponse implements Serializable {
    ArrayList<Genners>genres;

    public ArrayList<Genners> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genners> genres) {
        this.genres = genres;
    }
}
