package com.movieapp.qusai.ui.data.remote;

import com.movieapp.qusai.ui.trending_genners.GennerResponse;
import com.movieapp.qusai.ui.trending_genners.TrendingModel;
import com.movieapp.qusai.ui.movies.MovieResponse;
import com.movieapp.qusai.ui.star_cast.StarCaseResponse;
import com.movieapp.qusai.ui.utils.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET(Constants.TREDING_API)
    Call<TrendingModel> getTrendings();

    @GET(Constants.GENNERS)
    Call<GennerResponse> getGenners();

    @GET(Constants.GET_MOVIE_GENNER_ID)

    Call<MovieResponse> getMovies(@Query("api_key") String api, @Query("page")int page, @Query("with_genres")int with_genres);

    @GET(Constants.MOVIE_CAST)
    Call<StarCaseResponse> getStarCast();


}
