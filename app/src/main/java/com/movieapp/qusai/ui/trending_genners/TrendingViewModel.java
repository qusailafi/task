package com.movieapp.qusai.ui.trending_genners;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.movieapp.qusai.ui.data.remote.ApiClient;
import com.movieapp.qusai.ui.base.BaseViewModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrendingViewModel  extends BaseViewModel {
public  MutableLiveData<TrendingModel>trending;
public MutableLiveData<GennerResponse>genners;

public TrendingViewModel( ) {
         trending=new MutableLiveData<>(new TrendingModel());
    genners=new MutableLiveData<>(new GennerResponse());
    }
public void getGenners(){
    ApiClient.getInstance().getGenner().enqueue(new Callback<GennerResponse>() {
        @Override
        public void onResponse(Call<GennerResponse> call, Response<GennerResponse> response) {
            if (response.isSuccessful()){
                genners.setValue(response.body());
            }
        }

        @Override
        public void onFailure(Call<GennerResponse> call, Throwable t) {

        }
    });
}
    public void trendingApi(){
        ApiClient.getInstance().getApi( ).enqueue(new Callback<TrendingModel>() {
            @Override
            public void onResponse(Call<TrendingModel> call, Response<TrendingModel> response) {
                if (response.isSuccessful()){
                    trending.setValue(response.body());


                }
            }

            @Override
            public void onFailure(Call<TrendingModel> call, Throwable t) {
                Log.d("exception","exception");
            }
        });

    }
}
