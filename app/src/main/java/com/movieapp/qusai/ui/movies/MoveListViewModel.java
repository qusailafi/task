package com.movieapp.qusai.ui.movies;

import androidx.lifecycle.MutableLiveData;

import com.movieapp.qusai.ui.base.BaseViewModel;
import com.movieapp.qusai.ui.data.local.data.MovieMapper;

public class MoveListViewModel extends BaseViewModel {
    public Movie movie;
    public MutableLiveData<Boolean> isFavorite = new MutableLiveData(false);
MutableLiveData<Movie>movieLiveData=new MutableLiveData<>(new Movie());
public interface ClickListener{
 public    void onClick(Movie movie);
}
 public    ClickListener clickListener;
    public MoveListViewModel(Movie movie   ) {
        this.movie = movie;
         movieLiveData.postValue(movie);
         this.clickListener=clickListener;
     }

public void onMoveClik(Movie movie){
        this.clickListener.onClick(movie);
}
     public void checkFavorite() {

        Thread thread = new Thread() {
            @Override
            public void run() {
                if (movieDao.getAllMoviesFavourite(movie.getId()) != null)
                    isFavorite.postValue(true);
                else
                    isFavorite.postValue(false);
            }
        };
        thread.start();
    }

    public void addFavourite(Movie movie) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                movieDao.addFavorite(MovieMapper.covertToFavorite(movie));
                checkFavorite();
            }
        };

        thread.start();

    }

    public void deleteFavourite(Movie movie) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                movieDao.deleteFavorite(MovieMapper.covertToFavorite(movie));
                checkFavorite();
            }
        };

        thread.start();

    }
}
