package com.movieapp.qusai.ui.data.remote;

import com.movieapp.qusai.ui.trending_genners.GennerResponse;
import com.movieapp.qusai.ui.trending_genners.TrendingModel;
import com.movieapp.qusai.ui.movies.MovieResponse;
import com.movieapp.qusai.ui.star_cast.StarCaseResponse;
import com.movieapp.qusai.ui.utils.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static ApiClient instance;
    ApiService apiService;

    public ApiClient() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
.client(okHttpClient)
                .build();
        apiService = retrofit.create(ApiService.class);

    }

    public static ApiClient getInstance() {
        if (instance == null) {
            instance = new ApiClient();

        }
        return instance;
    }

    public Call<TrendingModel> getApi() {
        return apiService.getTrendings();
    }

    public Call<MovieResponse> getMovies(String apiKey,int page, int genner) {
        return apiService.getMovies( apiKey,page,genner);
    }

    public Call<GennerResponse> getGenner() {
        return apiService.getGenners();

    }
    public Call<StarCaseResponse>getStarCast(){
        return apiService.getStarCast();
    }
}
