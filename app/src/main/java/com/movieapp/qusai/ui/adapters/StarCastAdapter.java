package com.movieapp.qusai.ui.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.movieapp.qusai.databinding.StarCastRowBinding;
import com.movieapp.qusai.ui.star_cast.StarCastObject;
import com.movieapp.qusai.ui.utils.Constants;

import java.util.ArrayList;

public class StarCastAdapter  extends RecyclerView.Adapter<BaseViewHolder> {
ArrayList<StarCastObject>starCastObjects;

    public StarCastAdapter(ArrayList<StarCastObject> starCastObjects) {
        this.starCastObjects = starCastObjects;
    }


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        StarCastRowBinding starCastViewModel= StarCastRowBinding.inflate(LayoutInflater.from(parent.getContext())
                ,parent,false);
//        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.star_cast_row,parent,false);
        return new StarCastHolder(starCastViewModel);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }



    @Override
    public int getItemCount() {
        return starCastObjects.size();
    }

    public class  StarCastHolder extends BaseViewHolder{
StarCastRowBinding starCastRowBinding;
        public StarCastHolder(StarCastRowBinding starCastRowBinding) {
            super(starCastRowBinding.getRoot());
            this.starCastRowBinding = starCastRowBinding;
        }

        @Override
        public void onBind(int position) {
            starCastRowBinding.img.setImageURI(Constants.LOAD_IMAGE_URL+starCastObjects.get(getAdapterPosition()).getProfile_path());
        }
    }
}
