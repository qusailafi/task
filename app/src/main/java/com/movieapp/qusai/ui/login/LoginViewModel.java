package com.movieapp.qusai.ui.login;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.internal.$Gson$Preconditions;

import java.util.concurrent.Executor;

public class LoginViewModel extends AndroidViewModel {

    private FirebaseAuth auth;
    Application application;

    MutableLiveData<String> email, pass;
MutableLiveData<Boolean>isLoginSucess;
    public LoginViewModel(@NonNull Application app) {
        super(app);
        application = app;
        email = new MutableLiveData<>();
        isLoginSucess=new MutableLiveData<>(false);
        email.setValue("");
        pass = new MutableLiveData<>();

        pass.setValue("");
    }

    public void setEmail(String email) {
        this.email.setValue(email);
    }

    public void setPassword(String pass) {
        this.pass.setValue(pass);
    }
public LiveData<Boolean>isLoginSucess(){
        return isLoginSucess;
}
    public void login(String em, String password) {
        if (!em.isEmpty() && !password.isEmpty()) {

            auth = FirebaseAuth.getInstance();
            setEmail(em);
            setPassword(password);
            auth.signInWithEmailAndPassword(em, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                isLoginSucess.setValue(true);
                             } else {
                                isLoginSucess.setValue(false);

                                Toast.makeText(application, "Login failed! Please try again later", Toast.LENGTH_LONG).show();

                            }
                        }
                    });
        }

    }

}
