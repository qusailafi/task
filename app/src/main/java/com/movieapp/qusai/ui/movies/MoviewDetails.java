package com.movieapp.qusai.ui.movies;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;

import com.movieapp.qusai.R;
import com.movieapp.qusai.databinding.ActivityMoviewDetailsBinding;
import com.movieapp.qusai.ui.adapters.StarCastAdapter;
import com.movieapp.qusai.ui.base.BaseActivity;
import com.movieapp.qusai.ui.star_cast.StarCaseResponse;
import com.movieapp.qusai.ui.star_cast.StarCastObject;
import com.movieapp.qusai.ui.star_cast.StarCastViewModel;
import com.movieapp.qusai.ui.utils.Constants;

import java.util.ArrayList;

public class MoviewDetails extends BaseActivity {
    Movie movie;
    ActivityMoviewDetailsBinding binding;
    StarCastViewModel starCastViewModel;
    StarCastAdapter starCastAdapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        movie = (Movie) getIntent().getSerializableExtra("movie");

        binding = DataBindingUtil.setContentView(this, R.layout.activity_moview_details);
        starCastViewModel = ViewModelProviders.of(this).get(StarCastViewModel.class);
        starCastViewModel.getStarCast();
        binding.vote.setText(movie.getVote_average() + "");
        binding.desc.setText(movie.getOverview());
        binding.movieName.setText(movie.getOriginal_title());
        binding.img.setImageURI(Constants.LOAD_IMAGE_URL + movie.getPoster_path());
        binding.setCastViewModel(starCastViewModel);
        updateUi();


    }

    private void updateUi() {
        starCastViewModel.data.observe(this, new Observer<StarCaseResponse>() {
            @Override
            public void onChanged(StarCaseResponse starCaseResponse) {
                if (starCaseResponse.getCast() != null)
                    if (starCaseResponse.getCast().size() > 0) {
                        setAdapter(starCaseResponse.getCast());
                    }
            }
        });
    }

    private void setAdapter(ArrayList<StarCastObject> castObjects) {
        starCastAdapter = new StarCastAdapter(castObjects);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.castRv.setHasFixedSize(true);
        binding.castRv.setLayoutManager(linearLayoutManager);
        binding.castRv.setAdapter(starCastAdapter);
    }
}