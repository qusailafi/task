package com.movieapp.qusai.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.movieapp.qusai.R;
import com.movieapp.qusai.databinding.TrendingRowBinding;
import com.movieapp.qusai.ui.trending_genners.TrendingObject;
import com.movieapp.qusai.ui.utils.Constants;

import java.util.ArrayList;

public class  TrendingAdapter extends RecyclerView.Adapter<BaseViewHolder> {
Context context;
    ArrayList<TrendingObject> trendingModel;
public interface TrendingListener{
    void onTrendClick(TrendingObject trendingObject);
}
    TrendingListener trendingListener;
    public TrendingAdapter(Context context, ArrayList<TrendingObject> trendingModel) {
        this.context = context;
        this.trendingModel = trendingModel;
this.trendingModel=trendingModel;

    }


    @NonNull
    @Override
    public TrendingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TrendingRowBinding trendingRowBinding=TrendingRowBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent,false);
        return new TrendingHolder(trendingRowBinding) ;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }


    @Override
    public int getItemCount() {
        return trendingModel.size();
    }

    public class TrendingHolder extends BaseViewHolder{
        SimpleDraweeView img;
        TrendingRowBinding trendingRowBinding;

        public TrendingHolder(TrendingRowBinding trendingRowBinding) {
            super(trendingRowBinding.getRoot());
            img=  itemView.findViewById(R.id.img);
            this
                    .trendingRowBinding=trendingRowBinding;
        }

        @Override
        public void onBind(int position) {
            TrendingObject trendingObject=trendingModel.get(getAdapterPosition());
            trendingRowBinding.img.setImageURI(Constants.LOAD_IMAGE_URL+trendingObject.getPoster_path());

        }
    }
}
