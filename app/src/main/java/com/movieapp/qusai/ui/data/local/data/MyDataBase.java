package com.movieapp.qusai.ui.data.local.data;

import android.content.Context;


import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.movieapp.qusai.ui.movies.Favorite;

@Database(entities = Favorite.class,version = 2, exportSchema = false)
public abstract class MyDataBase extends RoomDatabase {
private static MyDataBase instance;
public abstract MovieDao movieDao();
public  static synchronized MyDataBase getInstance(Context context){
    if (instance==null){

        instance= Room.databaseBuilder(context, MyDataBase.class,"database")
.fallbackToDestructiveMigration()
                .build();
    }
    return instance;
}
}
