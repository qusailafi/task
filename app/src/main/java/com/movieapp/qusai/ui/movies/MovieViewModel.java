package com.movieapp.qusai.ui.movies;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.movieapp.qusai.ui.data.remote.ApiClient;
import com.movieapp.qusai.ui.base.BaseViewModel;
import com.movieapp.qusai.ui.utils.Constants;
import com.movieapp.qusai.ui.data.local.data.MovieMapper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieViewModel extends BaseViewModel {
    public MutableLiveData<MovieResponse> data;
 public MutableLiveData<Boolean>itemClick=new MutableLiveData<>(false);


    public MovieViewModel() {
        data = new MutableLiveData<>(new MovieResponse());
     }

    public void getMoviewGennerId(String apiKey, int page, int gennerId) {
        ApiClient.getInstance().getMovies(Constants.API_KEY, 1, gennerId).enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful()) {
                    data.setValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.d("ssss", "ssss");
            }
        });
    }

    public void addFavourite(Movie movie) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                movieDao.addFavorite(MovieMapper.covertToFavorite(movie));

            }
        };

        thread.start();

    }



}
