package com.movieapp.qusai.ui.adapters;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.movieapp.qusai.databinding.MoviesRowBinding;
import com.movieapp.qusai.ui.movies.MoveListViewModel;
import com.movieapp.qusai.ui.movies.Movie;
import com.movieapp.qusai.ui.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapterNew extends RecyclerView.Adapter<BaseViewHolder> implements View.OnClickListener {

    private List<Movie> movies;
    private Context context;

    @Override
    public void onClick(View view) {

    }


    public interface  ClickListener{
    void onClick(Movie movie);
}
    ClickListener clickListener;
    public MovieAdapterNew(Context context,List<Movie> movies,    ClickListener clickListener) {
        this.movies = movies;
        this.context = context;
        this.clickListener=clickListener;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MoviesRowBinding itemOfferBinding = MoviesRowBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new OfferViewHolder(itemOfferBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class OfferViewHolder extends BaseViewHolder {

        private MoviesRowBinding moviesRowBinding;
        private MoveListViewModel moveListViewModel;

        public OfferViewHolder(MoviesRowBinding moviesRowBinding) {
            super(moviesRowBinding.getRoot());
            this.moviesRowBinding = moviesRowBinding;
        }

        @Override
        public void onBind(int position) {
            Movie movie = movies.get(position);
            moveListViewModel = new MoveListViewModel(movie);
            moviesRowBinding.setViewmodel(moveListViewModel);

            moveListViewModel.checkFavorite();
moviesRowBinding.cardContainer.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        clickListener.onClick(movie);
    }
});
            moviesRowBinding.setLifecycleOwner((LifecycleOwner) context);
            moviesRowBinding.executePendingBindings();
            moviesRowBinding.img.setImageURI(Constants.LOAD_IMAGE_URL+movie.getPoster_path());
        }
    }


}
