package com.movieapp.qusai.ui.star_cast;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.movieapp.qusai.ui.data.remote.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class  StarCastViewModel extends AndroidViewModel {
    public MutableLiveData<StarCaseResponse>data;
    public StarCastViewModel(@NonNull Application application) {
        super(application);
        data=new MutableLiveData<>(new StarCaseResponse());
    }

    public  void getStarCast(){
        ApiClient.getInstance().getStarCast().enqueue(new Callback<StarCaseResponse>() {
            @Override
            public void onResponse(Call<StarCaseResponse> call, Response<StarCaseResponse> response) {
                if (response.isSuccessful())
                {
                    data.setValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<StarCaseResponse> call, Throwable t) {
                Log.d("sss","sss");
            }
        });
    }
}
