package com.movieapp.qusai.ui.home_page;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.Window;

import com.movieapp.qusai.R;
import com.movieapp.qusai.databinding.ActivityHomePageBinding;
import com.movieapp.qusai.ui.adapters.TrendingAdapter;
import com.movieapp.qusai.ui.base.BaseActivity;
import com.movieapp.qusai.ui.trending_genners.Genners;
import com.movieapp.qusai.ui.movies.MoviesFragment;
import com.movieapp.qusai.ui.trending_genners.GennerResponse;
import com.movieapp.qusai.ui.trending_genners.TrendingModel;
 import com.movieapp.qusai.ui.adapters.TabAdapter;
import com.movieapp.qusai.ui.trending_genners.TrendingViewModel;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class HomePage extends BaseActivity {
    TrendingViewModel trendingViewModel;
TrendingAdapter trendingAdapter;
LinearLayoutManager linearLayoutManager;
    ActivityHomePageBinding activityHomePageBinding;
    private TabAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


          activityHomePageBinding = DataBindingUtil.setContentView(this, R.layout.activity_home_page);

        trendingViewModel = ViewModelProviders.of(this).get(TrendingViewModel.class);

        trendingViewModel.trendingApi();
        trendingViewModel.getGenners();
        iniTrendingRvSettings();
        activityHomePageBinding.setTrndingViewModel(trendingViewModel);
      getTrending();
        getGenners();
    }
    private void getTrending(){
        trendingViewModel.trending.observe(this, new Observer<TrendingModel>() {
            @Override
            public void onChanged(TrendingModel trendingModel) {
                if (trendingModel.getResults() != null) {
                    setTrendingAdapter();
                }
            }
        });
    }
    private void getGenners(){
        trendingViewModel.genners.observe(this, new Observer<GennerResponse>() {
            @Override
            public void onChanged(GennerResponse gennerResponse) {
if (gennerResponse.getGenres()!=null)
    fillTabsAndPager();
            }
        });
    }

    private void fillTabsAndPager(){
        adapter = new TabAdapter(getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
 for (int x=0;x<trendingViewModel.genners.getValue().getGenres().size();x++){
     Genners genners=new Genners();
     genners=trendingViewModel.genners.getValue().getGenres().get(x);
     Bundle bundle=new Bundle();
     bundle.putSerializable("obj",genners);
     MoviesFragment moviesFragment=new MoviesFragment();
     moviesFragment.setArguments(bundle);
     adapter.addFragment(moviesFragment,trendingViewModel.genners.getValue().getGenres().get(x).getName());
 }
        activityHomePageBinding.viewPager.setAdapter(adapter);
        activityHomePageBinding.tabs.setupWithViewPager(activityHomePageBinding.viewPager);
     }
private void iniTrendingRvSettings(){
        activityHomePageBinding.trendingRv.setHasFixedSize(true);
        linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        activityHomePageBinding.trendingRv.setLayoutManager(linearLayoutManager);
}
    private void setTrendingAdapter(){
             trendingAdapter = new TrendingAdapter(HomePage.this, trendingViewModel.trending.getValue().getResults());
            activityHomePageBinding.trendingRv.setAdapter(trendingAdapter);


    }
}