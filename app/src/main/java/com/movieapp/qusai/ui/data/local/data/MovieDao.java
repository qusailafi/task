package com.movieapp.qusai.ui.data.local.data;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.movieapp.qusai.ui.movies.Favorite;
import com.movieapp.qusai.ui.movies.Movie;

import java.util.ArrayList;
import java.util.List;

import retrofit2.http.DELETE;

@Dao
public interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addFavorite(Favorite favorite);

    @Delete
    public void deleteFavorite(Favorite favorite);

    @Query("SELECT * FROM favorites where id=:id")
    Favorite getAllMoviesFavourite(int id);

}
