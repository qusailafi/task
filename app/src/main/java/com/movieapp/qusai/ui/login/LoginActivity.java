package com.movieapp.qusai.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.movieapp.qusai.R;
import com.movieapp.qusai.databinding.ActivityLoginBinding;
import com.movieapp.qusai.ui.base.BaseActivity;
import com.movieapp.qusai.ui.home_page.HomePage;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityLoginBinding activityLoginBinding= DataBindingUtil.setContentView(this,R.layout.activity_login);
        LoginViewModel loginViewModel= ViewModelProviders.of(this).get(LoginViewModel.class);
        loginViewModel.setEmail(loginViewModel.email.toString());
        loginViewModel.setPassword(activityLoginBinding.password.toString());
        activityLoginBinding.setLoginViewModel(loginViewModel);
        activityLoginBinding.setLifecycleOwner(this);
        loginViewModel.isLoginSucess().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean){
                    startActivity(new Intent(LoginActivity.this, HomePage.class));
                    finish();
                }
            }
        });


    }
}
