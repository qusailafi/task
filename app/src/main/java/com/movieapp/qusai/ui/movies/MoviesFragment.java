package com.movieapp.qusai.ui.movies;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.movieapp.qusai.R;
import com.movieapp.qusai.databinding.MoveiFragmentBinding;
 import com.movieapp.qusai.ui.adapters.MovieAdapterNew;
import com.movieapp.qusai.ui.trending_genners.Genners;
import com.movieapp.qusai.ui.utils.Constants;

import java.util.ArrayList;

public class MoviesFragment extends Fragment {
    MovieViewModel movieViewModel;
    MoveiFragmentBinding moveiFragmentBinding;
    GridLayoutManager gridLayoutManager;
    MovieAdapterNew movieAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        moveiFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.movei_fragment, container, false);
        movieViewModel = ViewModelProviders.of(getActivity()).get(MovieViewModel.class);
        moveiFragmentBinding.setMoveViewModel(movieViewModel);

        Bundle bundle = getArguments();
        Genners movie = (Genners) bundle.getSerializable("obj");
        movieViewModel.getMoviewGennerId(Constants.API_KEY, 1, movie.getId());
        moveiFragmentBinding.setLifecycleOwner(this);

        return moveiFragmentBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        movieViewModel.data.observe(this, new Observer<MovieResponse>() {
            @Override
            public void onChanged(MovieResponse movieResponse) {
                if (movieResponse != null)
                    if (movieResponse.results != null)
                        if (movieResponse.results.size() > 0)
                            setAdapter((ArrayList<Movie>) movieResponse.results);
            }
        });
    }

    private void setAdapter(ArrayList<Movie> movies) {


        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        moveiFragmentBinding.moviesRv.setHasFixedSize(true);
        moveiFragmentBinding.moviesRv.setLayoutManager(gridLayoutManager);
        movieAdapter = new MovieAdapterNew(getActivity(), movies, new MovieAdapterNew.ClickListener() {
            @Override
            public void onClick(Movie movie) {
                getActivity().startActivity(new Intent(getActivity(),MoviewDetails.class)
                .putExtra("movie",movie));
            }
        });

        moveiFragmentBinding.moviesRv.setAdapter(movieAdapter);


    }
}
