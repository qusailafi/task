package com.movieapp.qusai.ui.base;

import androidx.lifecycle.ViewModel;

import com.movieapp.qusai.ui.MyApp;
import com.movieapp.qusai.ui.data.local.data.MovieDao;
import com.movieapp.qusai.ui.data.local.data.MyDataBase;

public class BaseViewModel extends ViewModel {
    public MyDataBase myDataBase;
    public MovieDao movieDao;

    public BaseViewModel() {
        myDataBase = MyDataBase.getInstance(MyApp.getInstance());
        movieDao = myDataBase.movieDao();
    }


}
