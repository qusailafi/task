package com.movieapp.qusai.ui;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

public class MyApp extends Application {
    private static MyApp sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        init(this);
        Fresco.initialize(this);
    }
    private static void init(MyApp app) {
        sInstance = app;
    }

    public static MyApp getInstance() {
        return sInstance;
    }
}
