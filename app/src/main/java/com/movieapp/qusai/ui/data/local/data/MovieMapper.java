package com.movieapp.qusai.ui.data.local.data;

import com.movieapp.qusai.ui.movies.Favorite;
import com.movieapp.qusai.ui.movies.Movie;

public class MovieMapper {

    public static Favorite covertToFavorite(Movie movie){
        Favorite favorite=new Favorite();
        favorite.setId(movie.getId());

        return favorite;
    }

}
